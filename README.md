# age-estimation

In this repository three .ipynb notebooks are contained. The preprocessing and import of data is the same in all three of these.

_age_estimation_basic.ipynb_ contains the basic architecture inspired by AlexNet.

_age_estimation_weighted.ipynb_ contains the basic architecture with the modified, weighted loss function.

_age_estimation_inception.ipynb_ contains the modified architecture with the simple inception blocks together with normal MSE loss function.
